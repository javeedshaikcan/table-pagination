import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  ChangeDetectorRef
} from '@angular/core';
import { DataService } from './data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'tablePagination';
  @ViewChild('dataTable') dataTable: ElementRef;
  data: any;
  dataHeadings: string[];
  completeData: any;
  pages: any;
  selectedDropdownValue: any;
  selectedPage: any = 1;
  totalRows: any;
  constructor(private dataService: DataService) {}
  ngOnInit(): void {
    // get Data from json service.
    this.dataService.getData().subscribe(res => {
      if (res) {
        this.completeData = res; // store backup of complete data
        this.data = res; // this variable is used to bind the data on html page.
        this.totalRows = this.completeData.length; // total numbers of rows
        this.selectedDropdownValue = this.totalRows; // selected value of dropdown which will explain how many number of row to show.
        this.dataHeadings = Object.keys(res[0]); // variable used to bind header of table.
      }
    });
  }
  /**
   * event for dropdown selected.
   * @param selectedValue = selectedValue from dropdown.
   */
  onOptionsSelected(selectedValue: any) {
    this.selectedDropdownValue = selectedValue;
    this.data = this.completeData.slice(0, this.selectedDropdownValue); // slice the selected Values to in the table.
    if (this.totalRows > this.selectedDropdownValue) {
      const pagenum = Math.ceil(this.totalRows / this.selectedDropdownValue); // ceil total(rows/maxrows) to get ..
      this.pages = [...Array(pagenum)].map((_, i) => ++i); // Numbers of pages
    }
  }
  /**
   * Event for pagination select.
   * @param selectedPage = selected Pagination value.
   */
  selectPage(selectedPage) {
    this.selectedPage = selectedPage;
    this.data = this.completeData.slice(
      this.selectedDropdownValue * (selectedPage - 1),
      this.selectedDropdownValue * (selectedPage - 1) +
        +this.selectedDropdownValue
    ); // slice the selected Values to in the table.
  }
  /**
   * Each row select event.
   * @param row
   */
  onClickButton(row) {
    alert('Selected ID = ' + row.id + ' and status = ' + row.status);
  }
}
